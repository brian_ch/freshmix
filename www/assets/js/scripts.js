// Shopify scripts
(function () {
  var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
  if (window.ShopifyBuy) {
    if (window.ShopifyBuy.UI) {
      ShopifyBuyInit();
    } else {
      loadScript();
    }
  } else {
    loadScript();
  }

  function loadScript() {
    var script = document.createElement('script');
    script.async = true;
    script.src = scriptURL;
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
    script.onload = ShopifyBuyInit;
  }

  function ShopifyBuyInit() {
    var client = ShopifyBuy.buildClient({
      domain: 'drinkfreshmix.myshopify.com',
      apiKey: 'f47edd70548eb20bd5e74f4c0c0fa0d8',
      appId: '6',
    });

    var btnOptions =  {
      "product": {
        "iframe": false,
        "variantId": "all",
        "contents": {
          "img": false,
          "title": false,
          "price": false,
          "imgWithCarousel": false,
          "variantTitle": false,
          "description": false,
          "buttonWithQuantity": false,
          "quantity": false
        },
        "templates": {
          "button": '<button class="button buy bottom-spacing inverted">Buy single package</button>'
        },
        "classes": {
          "button": 'button buy bottom-spacing inverted',
        },
        "styles": {
          "product": {
            "@media (min-width: 601px)": {
              "max-width": "calc(25% - 20px)",
              "margin-left": "20px",
              "margin-bottom": "50px"
            }
          }
        }
      },
      "cart": {
        "contents": {
          "button": true
        },
        "styles": {
          "footer": {
            "background-color": "#ffffff"
          }
        }
      },
      "modalProduct": {
        "contents": {
          "img": false,
          "imgWithCarousel": true,
          "variantTitle": false,
          "buttonWithQuantity": true,
          "button": false,
          "quantity": false
        },
        "styles": {
          "product": {
            "@media (min-width: 601px)": {
              "max-width": "100%",
              "margin-left": "0px",
              "margin-bottom": "0px"
            }
          }
        }
      },
      "productSet": {
        "buttonDestination": 'modal',
        "styles": {
          "products": {
            "@media (min-width: 601px)": {
              "margin-left": "-20px"
            }
          }
        }
      }
    };

    // if(document.querySelectorAll('.shopify-buy-button').length == 0){
    //   btnOptions.product.contents.button = false;
    // }

    if(document.querySelectorAll('.buy-modal-button') > 0) {
      // var btnOptions = options;
      // btnOptions.product.contents.img = true;
      // btnOptions.product.contents.title = true;
      // btnOptions.product.contents.price = true;
    }

    ShopifyBuy.UI.onReady(client).then(function (ui) {
      if(document.querySelectorAll('.shopify-buy-button').length > 0){
        ui.createComponent('product', {
          id: 8934301005,
          node: document.getElementById('cucukiwi-product'),
          options: btnOptions
        });
        ui.createComponent('product', {
          id: 8934446669,
          node: document.getElementById('kalepine-product'),
          options: btnOptions
        });
      }
      if(document.querySelectorAll('.buy-modal-button').length > 0){
        ui.createComponent('collection', {
          id: 409174349,
          node: document.getElementById('modal-content'),
          options: {
            "product": {
              "iframe": false,
              "contents": {
                before: true,
                after: true
              },
              "classes": {
                "button": 'button bottom-spacing inverted',
              },
              "templates": {
                before: '<div class="product">',
                title: '<h3>{{data.title}}</h3>',
                price: '<h2>${{data.selectedVariant.price}}</h2>',
                button: '<button class="button buy bottom-spacing inverted">Buy single package</button>',
                after: '</div>'
              },
              "order": [
                'before',
                'img',
                'title',
                'price',
                'button',
                'after'
              ]
            },
            "productSet": {
              "iframe": false,
              "contents": {
                pagination: false
              }
            }
          }
        });
      }
    });
  }
})();

// Site scripts
$('header .hamburger').unbind('click').click(function(){
  $(this).parent().toggleClass('active');
});

$('.buy-modal-button').unbind('click').click(function(){
  $('body').addClass('modal-open');
  $('#modal').unbind('click').click(function(e){
    var $el = $(e.target);
    if($el.hasClass('close') || $el.is('#modal')) {
      $('body').removeClass('modal-open');
      $('#modal').unbind('click');
    }
  });
});
