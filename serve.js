'use strict'

const express = require('express');
const app = express();
const path = require('path');

let port = process.env.PORT || 8080;

app.use('/assets', express.static(path.join(__dirname, 'www/assets')))

app.get('/', (req, res) => {
    res.sendFile(path.join(`${__dirname}/www/index.html`))
})

app.get('/:file', (req, res) => {
    let file = req.params.file;
    if(file == 'flavors' || file == 'contact-us') {
      res.sendFile(path.join(`${__dirname}/www/${file}.html`))
    } else {
      res.redirect('/');
    }
})

app.listen(port)
