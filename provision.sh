#!/usr/bin/env bash

# rootpass=P2fjpoH9Nafp8Ar
# mysqldb=KeraDB
# mysqluser=KeraUser
# mysqlpass=7qfbQG0g3GEI

# install latest wordpress
#(cd /vagrant && wget https://wordpress.org/latest.zip && unzip -j latest.zip && rm latest.zip)

# disable ipv6
echo "
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf

# sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $rootpass"
# sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $rootpass"
apt-get update
apt-get install -y apache2 php5-memcached memcached ruby ruby-dev

# setup apache
rm -f /etc/apache2/sites-enabled/*

echo "
<VirtualHost *:80>
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /vagrant

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn, error, crit, alert, emerg.
        LogLevel info

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<Directory /vagrant>
        AllowOverride All
        Require all granted
</Directory>
" > /etc/apache2/sites-available/vagrant.conf

ln -s ../sites-available/vagrant.conf /etc/apache2/sites-enabled/vagrant.conf

sed -i '/short_open_tag = Off/c short_open_tag = On' /etc/php5/apache2/php.ini
a2enmod rewrite

service apache2 restart

# setup mysql user
# echo "CREATE USER '$mysqluser'@'localhost' IDENTIFIED BY '$mysqlpass'" | mysql -uroot -p$rootpass
# echo "CREATE DATABASE $mysqldb" | mysql -uroot -p$rootpass
# echo "GRANT ALL ON $mysqldb.* TO '$mysqluser'@'localhost'" | mysql -uroot -p$rootpass
# echo "flush privileges" | mysql -uroot -p$rootpass

# if mysqldump file exists, run it
# if [ -f /vagrant/mysqldump.sql ]; then
#     mysql -u root -p$rootpass $mysqldb < /vagrant/mysqldump.sql
# fi

# dump mysql every 10 minutes
# echo "*/10 * * * * mysqldump --user=$mysqluser -p$mysqlpass $mysqldb > /vagrant/mysqldump.sql" | crontab

# Install wordpress cli
# cd /tmp
# curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
# chmod +x wp-cli.phar
# sudo mv wp-cli.phar /usr/local/bin/wp

# Install Composer
# (cd /tmp && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer)
# composer install

# Install phpmyadmin
# apt-get -y install phpmyadmin
#
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-user string root"
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $rootpass"
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $rootpass"
# sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $rootpass"

# Install node and utilities
# apt-get -y install node
# npm install gulp

# Enable mcrypt
# php5enmod mcrypt
# echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
# service apache2 restart

# Install various utilities
# apt-get install -y htop nmap git
