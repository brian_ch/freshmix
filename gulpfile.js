'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')
const browserSync = require('browser-sync')

gulp.task('css', () => {
  return gulp.src('./scss/index.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./www/assets/css'))
    .pipe(browserSync.stream());
})

gulp.task('watch', () => {
  gulp.watch('./scss/partials/**', ['css'])
  gulp.watch('www/*.html').on('change', browserSync.reload);
  gulp.watch('www/assets/js/*').on('change', browserSync.reload);
})

gulp.task('serve', ['watch'], () => {
  browserSync.init({
    server: "./www"
  })
})

gulp.task('default', ['serve']);
